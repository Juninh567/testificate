minetest.register_node("trees:tree", {
    description = "Tree",
    tiles = {
        "trees_tree_top_and_bottom.png",    -- y+
        "trees_tree_top_and_bottom.png",  -- y-
        "trees_tree.png", -- x+
        "trees_tree.png",  -- x-
        "trees_tree.png",  -- z+
        "trees_tree.png", -- z-
    },
    is_ground_content = true,
    groups = {oddly_breakable_by_hand=2, wood=3, fuel=1},
})
minetest.register_node("trees:leaves", {
    description = "Leaves",
    tiles = {"trees_leaves.png"},
    is_ground_content = true,
    groups = {instant=1}
})
minetest.register_decoration({
    deco_type = "schematic",
    place_on = {"testificate:grass"},
    sidelen = 16,
    fill_ratio = 0.01,
    biomes = {"grasslands"},
    y_max = 270,
    y_min = 1,
    schematic = minetest.get_modpath("trees") .. "/schematics/tree.mts",
    flags = "place_center_x, place_center_z",
    rotation = "random",
})
minetest.register_node("trees:wood", {
    description = "Wood",
    tiles = {"trees_wood.png"},
    is_ground_content = true,
    groups = {oddly_breakable_by_hand=2, wood=3, fuel=1},
})
minetest.register_craft({
    type = "shapeless",
    output = "trees:wood 4",
    recipe = {
        "trees:tree",
    },
})
print("[TESTIFICATE] Trees loaded successfully")