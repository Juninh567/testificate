minetest.register_node("stairs:stair_stone", {
    description = "Stone Stair",
    tiles = {"stairs_stone.png"},
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
            {-0.5, 0, 0, 0.5, 0.5, 0.5},
        },
    },
    groups = {stone=3},
})
minetest.register_craft({
    type = "shaped",
    output = "stairs:stair_stone 6",
    recipe = {
        {"testificate:stone", "", ""},
        {"testificate:stone", "testificate:stone",  ""},
        {"testificate:stone", "testificate:stone",  "testificate:stone"}
    }
})
print("[TESTIFICATE] Stairs loaded successfully")