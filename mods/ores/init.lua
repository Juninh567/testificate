minetest.register_node("ores:stone_with_coal", {
    description = "Stone with Coal",
    tiles = {"ores_stone_with_coal.png"},
    is_ground_content = true,
    groups = {stone=3},
    drop = "ores:coal"
})
minetest.register_node("ores:stone_with_iron", {
    description = "Stone with Iron",
    tiles = {"ores_stone_with_iron.png"},
    is_ground_content = true,
    groups = {iron=3}
})
minetest.register_craftitem("ores:coal", {
    description = "Coal",
    inventory_image = "ores_coal.png",
    groups = {fuel=1}
})
minetest.register_craftitem("ores:iron", {
    description = "Iron",
    inventory_image = "ores_iron.png"
})
minetest.register_ore({

   ore_type       = "scatter",
   ore            = "ores:stone_with_coal",
   wherein        = "testificate:stone",
   clust_scarcity = 8*8*8,
   clust_num_ores = 8,
   clust_size     = 3,
   y_min     = -31000,
   y_max     = 64,

})
minetest.register_ore({

   ore_type       = "scatter",
   ore            = "ores:stone_with_iron",
   wherein        = "testificate:stone",
   clust_scarcity = 8*8*8,
   clust_num_ores = 8,
   clust_size     = 3,
   y_min     = -31000,
   y_max     = 64,

})
minetest.register_craft({
    type = "shapeless",
    output = "ores:iron",
    recipe = {"group:fuel", "ores:stone_with_iron"}
})
print("[TESTIFICATE] Ores loaded succesfully")