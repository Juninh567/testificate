minetest.register_node("util:torch", {
    description = "Torch",
    drawtype = "plantlike",

    -- Only one texture used
    tiles = {"util_torch.png"},
    light_source = 12,
    walkable = false,
    groups = {instant=3},
    selection_box = {
        type = "fixed",
        fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, 0.5, 6 / 16},
    },
})
minetest.register_craft({
    output = "util:torch 4",
    recipe = {
        {"ores:coal"},
        {"testificate:stick"},
    }
})
print("[TESTIFICATE] Util loaded successfully")