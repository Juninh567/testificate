-- Created by Juninho.
minetest.register_node("testificate:stone", {
    description = "Stone",
    tiles = {"testificate_stone.png"},
    is_ground_content = true,
    groups = {stone=3},
})
minetest.register_node("testificate:dirt", {
    description = "Dirt",
    tiles = {"testificate_dirt.png"},
    is_ground_content = true,
    groups = {snappy = 3}
})
minetest.register_node("testificate:grass", {
    description = "Grass",
    tiles = {
        "testificate_green.png",    -- y+
        "testificate_dirt.png",  -- y-
        "testificate_grass.png", -- x+
        "testificate_grass.png",  -- x-
        "testificate_grass.png",  -- z+
        "testificate_grass.png", -- z-
    },
    is_ground_content = true,
    groups = {snappy = 3},
    drop = "testificate:dirt"
})
minetest.register_node("testificate:water_source", {
	description = "Water Source",
	drawtype = "liquid",
	waving = 3,
	tiles = {
		{
			name = "testificate_water.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
		{
			name = "testificate_water.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	use_texture_alpha = "blend",
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	liquidtype = "source",
	liquid_alternative_flowing = "testificate:water_flowing",
	liquid_alternative_source = "testificate:water_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
})

minetest.register_node("testificate:water_flowing", {
	description = "Flowing Water",
	drawtype = "flowingliquid",
	waving = 3,
	tiles = {"testificate_water.png"},
	special_tiles = {
		{
			name = "testificate_water.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5,
			},
		},
		{
			name = "testificate_water.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5,
			},
		},
	},
	use_texture_alpha = "blend",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	groups = {not_in_creative_inventory=1},
	is_ground_content = false,
	drop = "",
	liquidtype = "flowing",
	liquid_alternative_flowing = "testificate:water_flowing",
	liquid_alternative_source = "testificate:water_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
})
minetest.register_biome({
    name = "grasslands",
    node_top = "testificate:grass",
    depth_top = 1,
    node_filler = "testificate:dirt",
    depth_filler = 4,
    y_max = 1000,
    y_min = -3,
    heat_point = 50,
    humidity_point = 50,
})
minetest.register_alias("mapgen_stone", "testificate:stone")
minetest.register_alias("mapgen_water_source", "testificate:water_source")
minetest.register_alias("mapgen_river_water_source", "testificate:water_source")

minetest.register_craftitem("testificate:stick", {
    description = "Stick",
    inventory_image = "testificate_stick.png"
})
minetest.register_craft({
    type = "shaped",
    output = "testificate:axe_wood",
    recipe = {
        {"trees:wood", "trees:wood", ""},
        {"trees:wood", "testificate:stick",  ""},
        {"", "testificate:stick",  ""}
    }
})
minetest.register_craft({
    type = "shapeless",
    output = "testificate:stick 4",
    recipe = {
        "trees:wood",
    },
})
minetest.register_tool("testificate:axe_wood", {
    description = "Wood Axe",
    inventory_image = "testificate_axe_wood.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            wood = {
                maxlevel = 2,
                uses = 50,
                times = { [1]=0.90, [2]=0.60, [3]=0.30 }
            },
        },
        damage_groups = {fleshy=2},
    },
})
minetest.register_tool("testificate:pick_wood", {
    description = "Wood Pick",
    inventory_image = "testificate_pick_wood.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            stone = {
                maxlevel = 2,
                uses = 50,
                times = { [1]=1.10, [2]=0.90, [3]=0.50 }
            },
        },
        damage_groups = {fleshy=2},
    },
})
minetest.register_tool("testificate:pick_stone", {
    description = "Stone Pick",
    inventory_image = "testificate_pick_stone.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            stone = {
                maxlevel = 2,
                uses = 50,
                times = { [1]=0.90, [2]=0.40, [3]=0.20 },
            },
	    iron = {
                maxlevel = 2,
                uses = 50,
                times = { [1]=1.10, [2]=0.90, [3]=0.50 },
            },
        },
        damage_groups = {fleshy=2},
    },
})
minetest.register_craft({
    type = "shaped",
    output = "testificate:pick_wood",
    recipe = {
        {"trees:wood", "trees:wood", "trees:wood"},
        {"", "testificate:stick",  ""},
        {"", "testificate:stick",  ""}
    }
})
minetest.register_craft({
    type = "shaped",
    output = "testificate:pick_stone",
    recipe = {
        {"testificate:stone", "testificate:stone", "testificate:stone"},
        {"", "testificate:stick",  ""},
        {"", "testificate:stick",  ""},
    }
})
print("[TESTIFICATE] Main mod loaded successfully")